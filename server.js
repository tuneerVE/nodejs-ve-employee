'use strict';

let express = require('express'),
    bodyParser = require('body-parser'),
    compression = require('compression'),
    cors = require('cors'),
    employees = require('./services/employee-service'),
    app = express();

app.set('port', process.env.PORT || 3000);

app.use(cors());
app.use(bodyParser.json());
app.use(compression());

//Route to get all the employee records
app.get('/employees', employees.findAll);

//Route to get employee records according to id
app.get('/employees/:id', employees.findById);

app.listen(app.get('port'), function () {
    console.log('Listening on port ' + app.get('port'));
});