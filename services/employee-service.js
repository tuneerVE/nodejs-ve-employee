'use strict';
//Employees records
let employees = [
    {
        id: 1,
        firstName: "Employee",
        lastName: "1",
        title: "CEO",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "amy@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 2,
        firstName: "Employee",
        lastName: "2",
        title: "VP of Engineering",
        managerId: 1,
        managerName: "Tuneer",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "anup@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 3,
        firstName: "Employee",
        lastName: "3",
        title: "VP of Marketing",
        managerId: 1,
        managerName: "Tuneer",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "michael@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 4,
        firstName: "Employee",
        lastName: "4",
        title: "VP of Sales",
        managerId: 1,
        managerName: "Tuneer",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "caroline@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 5,
        firstName: "Employee",
        lastName: "5",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "james@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 6,
        firstName: "Employee",
        lastName: "6",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "jen@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 7,
        firstName: "Employee",
        lastName: "7",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "jonathan@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 8,
        firstName: "Employee",
        lastName: "8",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "kenneth@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 9,
        firstName: "Employee",
        lastName: "9",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "lisa@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 10,
        firstName: "Employee",
        lastName: "10",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "brad@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 11,
        firstName: "Employee",
        lastName: "11",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "michelle@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 12,
        firstName: "Employee",
        lastName: "12",
        title: "Marketing Manager",
        managerId: 3,
        managerName: "Michael Jones",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "miriam@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 13,
        firstName: "Employee",
        lastName: "13",
        title: "Marketing Manager",
        managerId: 3,
        managerName: "Michael Jones",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "olivia@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 14,
        firstName: "Employee",
        lastName: "14",
        title: "Marketing Manager",
        managerId: 3,
        managerName: "Michael Jones",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "robert@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 15,
        firstName: "Employee",
        lastName: "15",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "tammy@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 16,
        firstName: "Employee",
        lastName: "16",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "123-456-7891",
        mobilePhone: "123-456-7891",
        email: "victor@virtual_employee.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    }
];

//Get all records or filter according to the name
exports.findAll = (req, res, next) => {
    let name = req.query.name;
    if (name) {
        let result = employees.filter(employee =>
            (employee.firstName +  ' ' + employee.lastName).toUpperCase().indexOf(name.toUpperCase()) > -1);
        return res.json(result);
    } else {
        return res.json(employees);
    }
};

//Get records according to the id
exports.findById = (req, res, next) => {
    let id = req.params.id;
    let employee = employees[id - 1];
    res.json({
        firstName: employee.firstName,
        lastName: employee.lastName,
        title: employee.title,
        email: employee.email,
        mobilePhone: employee.mobilePhone,
        picture: employee.picture,
        manager: employees[employee.managerId - 1],
        reports: employees.filter(item => item.managerId == id)
    });
}